<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use App\User;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::guard('api')->id()) {
            $is_exists = User::where('id' , Auth::guard('api')->id())->exists();
            
            $user = User::find(Auth::guard('api')->id());

            if (Auth::guard('api')->check() && $user->type == 1)
            {
                return $next($request);
            } else {
                $message = ["message" => "Permission Denied"];
                return response($message, 401);
            }

        } else {
            $message = ["message" => "Invalid token"];
                return response($message, 404);
        }
        
    }
}
