<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new User();
        $role_admin->name = 'Admin';
        $role_admin->email = 'admin@admin.com';
        $role_admin->password = bcrypt('secret');
        $role_admin->status = 1;
        $role_admin->type = 1;
        $role_admin->save();
    }
}
